﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace OrderManagementApp.Models
{
    public class Order: BaseModel
    {
        [Required()]
        public string Reciepnt { get; set; }
        public string Supplier { get; set; }
        [Required()]
        public string ProductName { get; set; }
        public DateTime? Send { get; set; }
        public DateTime? Delivered { get; set; }
        [Required()]
        public int AddressId { get; set; }
        public Address Address { get; set; }
    }

    // TODO: create Product model and link it to Order (many-one)
}
