﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace OrderManagementApp.Models
{
    public class Address: BaseModel
    {
        [Required()]
        public string Country { get; set; }
        [Required()]
        public string City { get; set; }
        [Required()]
        public string State { get; set; }
        [Required()]
        public string Street { get; set; }
        [Required()]
        public int PostalCode { get; set; }
        public ICollection<Order> Orders { get; set; }
    }
}
