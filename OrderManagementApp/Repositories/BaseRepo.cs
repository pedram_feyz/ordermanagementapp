﻿using Microsoft.EntityFrameworkCore;
using OrderManagementApp.Data;
using OrderManagementApp.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OrderManagementApp.Repositories
{
    public class BaseRepo<TEntity> : IBaseRepo<TEntity> where TEntity : class, IEntity
    {
        private readonly DbSet<TEntity> _dbSetContext;
        private readonly DataContext _context;
        public BaseRepo(DataContext context)
        {
            _context = context;
            _dbSetContext = _context.Set<TEntity>();
        }

        public async Task<TEntity> Create(TEntity entity)
        {
            var createdEntity = await _dbSetContext.AddAsync(entity);
            return (await SaveAll()) ? createdEntity.Entity : null;
        }

        public async Task<bool> Delete(TEntity entity)
        {
            _dbSetContext.Remove(entity);
            return await SaveAll();
        }

        public IQueryable<TEntity> GetAll()
        {
            //TODO add pagination
            var entities = _dbSetContext.Select(x => x);
            return entities;
        }

        public async Task<TEntity> Get(int id)
        {
            var entity = await _dbSetContext.FirstOrDefaultAsync(x => x.Id == id);
            return entity;
        }

        public IQueryable<TEntity> GetQuery()
        {
            var entity = _dbSetContext.Select(x => x);
            return entity;
        }

        public async Task<bool> SaveAll()
        {
            return await _context.SaveChangesAsync() > 0;
        }
    }
}
