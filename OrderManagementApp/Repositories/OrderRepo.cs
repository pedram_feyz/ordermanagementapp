﻿using Microsoft.EntityFrameworkCore;
using OrderManagementApp.Data;
using OrderManagementApp.Interfaces;
using OrderManagementApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OrderManagementApp.Repositories
{
    public class OrderRepo : BaseRepo<Order>, IOrderRepo
    {
        public OrderRepo(DataContext context) : base(context)
        {

        }
        public new async Task<List<Order>> GetAll()
        {
            //TODO add pagination
            var queryOrder = this.GetQuery();
            var orders = await queryOrder.ToListAsync();
            return orders;
        }

        public new IQueryable<Order> GetQuery()
        {
            //TODO add pagination
            var orderQuery = base.GetQuery();
            var orders = orderQuery.Include(o => o.Address).AsQueryable();
            return orders;
        }
    }
}
