﻿using Microsoft.EntityFrameworkCore;
using OrderManagementApp.Data;
using OrderManagementApp.Interfaces;
using OrderManagementApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OrderManagementApp.Repositories
{
    public class AddressRepo: BaseRepo<Address>, IAddressRepo
    {
        public AddressRepo(DataContext context) :base(context)
        {

        }

        public new async Task<Address> Get(int id)
        {
            var address = await base.Get(id);
            return address;
        }

        public new async Task<List<Address>> GetAll()
        {
            //TODO add pagination
            var queryAddresses = base.GetAll();
            var addresses = await queryAddresses.ToListAsync();
            return addresses;
        }

        public new async Task<Address> Create(Address address)
        {
            var createdAssress = await base.Create(address);
            return createdAssress;
        }

        public new async Task<bool> Delete(Address address)
        {
            return await base.Delete(address);
        }

        public new async Task<bool> SaveAll()
        {
            return await base.SaveAll();
        }
    }
}
