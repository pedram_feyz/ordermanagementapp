﻿using OrderManagementApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OrderManagementApp.Interfaces
{
    public interface IOrderRepo
    {
        Task<Order> Get(int id);
        Task<List<Order>> GetAll();
        Task<Order> Create(Order order);
        Task<bool> Delete(Order order);
        Task<bool> SaveAll();
        IQueryable<Order> GetQuery();
    }
}
