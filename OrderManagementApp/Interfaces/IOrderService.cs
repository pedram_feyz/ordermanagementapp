﻿using OrderManagementApp.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OrderManagementApp.Interfaces
{
    public interface IOrderService
    {
        Task<OrderDto> Get(int id);
        Task<List<OrderDto>> GetAll();
        Task<OrderDto> Create(OrderDto address);
        Task<OrderDto> Update(int id, OrderDto address);
        Task<bool> Delete(int id);
    }
}
