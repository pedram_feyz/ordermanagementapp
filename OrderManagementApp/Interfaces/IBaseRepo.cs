﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OrderManagementApp.Interfaces
{
    public interface IBaseRepo<TEntity> where TEntity : class
    {
        IQueryable<TEntity> GetAll();
        Task<TEntity> Get(int id);
        Task<TEntity> Create(TEntity entity);
        Task<bool> Delete(TEntity entity);
        Task<bool> SaveAll();
        IQueryable<TEntity> GetQuery();
    }
}
