﻿using OrderManagementApp.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OrderManagementApp.Interfaces
{
    public interface IAddressService
    {
        Task<AddressDto> Get(int id);
        Task<List<AddressDto>> GetAll();
        Task<AddressDto> Create(AddressDto address);
        Task<AddressDto> Update(int id, AddressDto address);
        Task<bool> Delete(int id);
    }
}
