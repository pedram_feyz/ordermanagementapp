﻿
using OrderManagementApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OrderManagementApp.Interfaces
{
    public interface IAddressRepo
    {
        Task<Address> Get(int id);
        Task<List<Address>> GetAll();
        Task<Address> Create(Address Address);
        Task<bool> Delete(Address Address);
        Task<bool> SaveAll();
    }
}
