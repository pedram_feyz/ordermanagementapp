﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace OrderManagementApp.Dtos
{
    public class OrderDto
    {
        [Required()]
        public int Id { get; set; }
        [Required()]
        public string Reciepnt { get; set; }
        public string Supplier { get; set; }
        [Required()]
        public string ProductName { get; set; }
        public DateTime? DispatchDate { get; set; }
        public DateTime? DeliveryDate { get; set; }
        public string AddressOfDelivery { get; set; }
        [Required()]
        [Range(1, int.MaxValue, ErrorMessage = "Address can not be empty.")]
        public int AddressId { get; set; }
    }
}
