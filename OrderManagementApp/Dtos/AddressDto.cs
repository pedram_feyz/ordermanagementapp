﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace OrderManagementApp.Dtos
{
    public class AddressDto
    {
        [Required()]
        public int Id { get; set; }
        [Required()]
        public string Country { get; set; }
        [Required()]
        public string City { get; set; }
        [Required()]
        public string State { get; set; }
        [Required()]
        public string Street { get; set; }
        [Required()]
        [Range(1000, 9999, ErrorMessage = "Postal Code is not correct or empty.")]
        public int PostalCode { get; set; }
        public string Address { get; set; }
    }
}
