﻿using OrderManagementApp.Interfaces;
using OrderManagementApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using AutoMapper;
using OrderManagementApp.Dtos;

namespace OrderManagementApp.Services
{
    public class AddressService : IAddressService
    {
        private readonly IAddressRepo _addressRepo;
        private readonly IMapper _mapper;
        public AddressService(IAddressRepo addressRepo, IMapper mapper)
        {
            _addressRepo = addressRepo;
            _mapper = mapper;
        }

        public async Task<AddressDto> Create(AddressDto addressDto)
        {
            var address = _mapper.Map<Address>(addressDto);
            var createdAddress =  await _addressRepo.Create(address);
            if (createdAddress == null)
            {
                throw new Exception("Faild to create the address.");
            }
            return _mapper.Map<AddressDto>(createdAddress);
        }

        public async Task<bool> Delete(int id)
        {
            var address = await _addressRepo.Get(id);
            if (address == null)
            {
                // return NotFound($"Could not find user with an Id if {id}");
                throw new Exception($"Could not find address with an Id: {id}");
            }

            if (await _addressRepo.Delete(address)) {
                return true;
            }
            throw new Exception($"Could not delete address with an Id: {id}");
        }

        public async Task<AddressDto> Get(int id)
        {
            var address = await _addressRepo.Get(id);
            if (address == null)
            {
                // return NotFound($"Could not find user with an Id if {id}");
                throw new Exception($"Could not find address with an Id: {id}");
            }
            var addressDto = _mapper.Map<AddressDto>(address);
            return addressDto;
        }

        public async Task<List<AddressDto>> GetAll()
        {
            var addresses = await _addressRepo.GetAll();
            var addressesDto = _mapper.Map<List<AddressDto>>(addresses);

            return addressesDto;
        }

        public async Task<AddressDto> Update(int id, AddressDto address)
        {
            var entity = await _addressRepo.Get(id);
            if (entity == null)
            {
                throw new Exception($"Could not find address with an Id: {id}");
            }
            _mapper.Map<AddressDto, Address>(address, entity);

            if (!await _addressRepo.SaveAll())
            {
                // return NotFound($"Could not find user with an Id if {id}");
                throw new Exception($"Could not update address with an Id: {id}");
            }
            // TODO: if it in needed, get updated entity and return it back
            address.Id = entity.Id;
            return address;
        }
    }
}
