﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using OrderManagementApp.Dtos;
using OrderManagementApp.Interfaces;
using OrderManagementApp.Models;

namespace OrderManagementApp.Services
{
    public class OrderService : IOrderService
    {
        private readonly IOrderRepo _orderRepo;
        private readonly IMapper _mapper;

        public OrderService(IOrderRepo orderRepo, IMapper mapper)
        {
            _orderRepo = orderRepo;
            _mapper = mapper;
        }
        public async Task<OrderDto> Create(OrderDto orderDto)
        {
            var order = _mapper.Map<Order>(orderDto);
            var createdOrder = await _orderRepo.Create(order);
            if (createdOrder == null)
            {
                throw new Exception("Faild to create the order.");
            }

            // include address 
            return await this.Get(createdOrder.Id);
            //return _mapper.Map<OrderDto>(createdOrder);
        }

        public async Task<bool> Delete(int id)
        {
            var order = await _orderRepo.Get(id);
            if (order == null)
            {
                throw new Exception($"Could not find order with an Id: {id}");
            }
            if (await _orderRepo.Delete(order))
            {
                return true;
            }
            throw new Exception($"Could not delete order with an Id: {id}");
        }

        public async Task<OrderDto> Get(int id)
        {
            //var order = await _orderRepo.Get(id);
            var order = await _orderRepo.GetQuery().FirstOrDefaultAsync(o => o.Id == id);
            if (order == null)
            {
                throw new Exception($"Could not find order with an Id: {id}");
            }
            var orderDto = _mapper.Map<OrderDto>(order);
            return orderDto;
        }

        public async Task<List<OrderDto>> GetAll()
        {
            var orders = await _orderRepo.GetAll();
            var ordersDto = _mapper.Map<List<OrderDto>>(orders);

            return ordersDto;
        }

        public async Task<OrderDto> Update(int id, OrderDto orderDto)
        {
            var entity = await _orderRepo.Get(id);
            if (entity == null)
            {
                throw new Exception($"Could not find order with an Id: {id}");
            }
            _mapper.Map<OrderDto, Order>(orderDto, entity);

            if (!await _orderRepo.SaveAll())
            {
                throw new Exception($"Could not update order with an Id: {id}");
            }
            return await this.Get(id);
        }
    }
}
