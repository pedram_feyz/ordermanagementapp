﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OrderManagementApp.Migrations
{
    public partial class Seeding : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Addresses",
                columns: new[] { "Id", "City", "Country", "PostalCode", "State", "Street" },
                values: new object[,]
                {
                    { 1, "Sydney", "Aus", 2000, "NSW", "305 kent st" },
                    { 2, "Melbourne", "Aus", 3000, "VIC", "200 sprint st" },
                    { 3, "Tasmania", "Aus", 4000, "TAS", "95 blue st" },
                    { 4, "London", "England", 1324, "LO", "4 cheer st" },
                    { 5, "Manchester", "England", 3456, "MN", "23 bill st" },
                    { 6, "New Yourk", "USA", 1040, "NY", "30 queen st" },
                    { 7, "Washington", "USA", 1250, "WA", "12 dan st" },
                    { 8, "Washington", "USA", 1340, "WA", "305 man st" },
                    { 9, "Paris", "France", 2345, "PA", "45 lo st" },
                    { 10, "Paris", "France", 2060, "PA", "3 prade st" },
                    { 11, "Madrid", "Spain", 3456, "MA", "67 bird st" },
                    { 12, "Barselona", "Spain", 4223, "BA", "23 bar st" }
                });

            migrationBuilder.InsertData(
                table: "Orders",
                columns: new[] { "Id", "AddressId", "Delivered", "ProductName", "Reciepnt", "Send", "Supplier" },
                values: new object[,]
                {
                    { 1, 1, new DateTime(2005, 9, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "laptop", "Pedram Feyz", new DateTime(2005, 8, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Asus" },
                    { 10, 1, null, "phone", "Pedram feyz", new DateTime(2019, 7, 7, 0, 0, 0, 0, DateTimeKind.Unspecified), "Nokia" },
                    { 2, 2, new DateTime(2015, 9, 3, 0, 0, 0, 0, DateTimeKind.Unspecified), "laptop", "Alice Gen", new DateTime(2015, 8, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Dell" },
                    { 3, 2, new DateTime(2005, 3, 7, 0, 0, 0, 0, DateTimeKind.Unspecified), "laptop", "Joe Bell", new DateTime(2018, 3, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Asus" },
                    { 4, 2, new DateTime(2005, 9, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), "bag", "Amit Doe", new DateTime(2019, 9, 11, 0, 0, 0, 0, DateTimeKind.Unspecified), "Sap" },
                    { 5, 3, new DateTime(2018, 9, 11, 0, 0, 0, 0, DateTimeKind.Unspecified), "bag", "Monty Cont", new DateTime(2018, 8, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "LuisVitton" },
                    { 11, 3, null, "bottel", "Mary Shel", null, "Bav" },
                    { 6, 4, new DateTime(2005, 9, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "shoes", "Richard Wiil", new DateTime(2005, 8, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Nike" },
                    { 7, 5, new DateTime(2015, 9, 18, 0, 0, 0, 0, DateTimeKind.Unspecified), "t-shirt", "Brad Pitt", new DateTime(2015, 8, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Adidas" },
                    { 8, 6, new DateTime(2005, 5, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "tabel", "Dan Leo", new DateTime(2009, 4, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Sed" },
                    { 9, 7, null, "tabel", "Matt Deimon", new DateTime(2019, 3, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Sed" },
                    { 12, 8, null, "towel", "Sara Kenz", null, "Sor" }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Addresses",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "Addresses",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "Addresses",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "Addresses",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "Orders",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Orders",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Orders",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Orders",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Orders",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Orders",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Orders",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "Orders",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "Orders",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "Orders",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "Orders",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "Orders",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "Addresses",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Addresses",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Addresses",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Addresses",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Addresses",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Addresses",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Addresses",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "Addresses",
                keyColumn: "Id",
                keyValue: 8);
        }
    }
}
