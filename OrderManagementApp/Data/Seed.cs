﻿using Microsoft.EntityFrameworkCore;
using OrderManagementApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OrderManagementApp.Data
{
    public static class Seed
    {
        public static void Seeding(this ModelBuilder modelBuilder)
        {
            var addresses = new List<Address>()
            {
                new Address { Id= 1, Country="Aus", City="Sydney", Street="305 kent st", State="NSW", PostalCode= 2000 },
                new Address { Id= 2, Country="Aus", City="Melbourne", Street="200 sprint st", State="VIC", PostalCode= 3000 },
                new Address { Id= 3, Country="Aus", City="Tasmania", Street="95 blue st", State="TAS", PostalCode= 4000 },
                new Address { Id= 4, Country="England", City="London", Street="4 cheer st", State="LO", PostalCode= 1324 },
                new Address { Id= 5, Country="England", City="Manchester", Street="23 bill st", State="MN", PostalCode= 3456 },
                new Address { Id= 6, Country="USA", City="New Yourk", Street="30 queen st", State="NY", PostalCode= 1040 },
                new Address { Id= 7, Country="USA", City="Washington", Street="12 dan st", State="WA", PostalCode= 1250 },
                new Address { Id= 8, Country="USA", City="Washington", Street="305 man st", State="WA", PostalCode= 1340 },
                new Address { Id= 9, Country="France", City="Paris", Street="45 lo st", State="PA", PostalCode= 2345 },
                new Address { Id= 10, Country="France", City="Paris", Street="3 prade st", State="PA", PostalCode= 2060 },
                new Address { Id= 11, Country="Spain", City="Madrid", Street="67 bird st", State="MA", PostalCode= 3456 },
                new Address { Id= 12, Country="Spain", City="Barselona", Street="23 bar st", State="BA", PostalCode= 4223 },
            };

            modelBuilder.Entity<Address>().HasData(addresses);

            var orders = new List<Order>()
            {
                new Order {Id = 1, ProductName="laptop", Reciepnt="Pedram Feyz", Supplier="Asus", Send=DateTime.Parse("2005-08-01") , Delivered=DateTime.Parse("2005-09-01"), AddressId = 1},
                new Order {Id = 2, ProductName="laptop", Reciepnt="Alice Gen", Supplier="Dell", Send=DateTime.Parse("2015-08-01") , Delivered=DateTime.Parse("2015-09-03"), AddressId = 2},
                new Order {Id = 3, ProductName="laptop", Reciepnt="Joe Bell", Supplier="Asus", Send=DateTime.Parse("2018-03-01") , Delivered=DateTime.Parse("2005-03-07"), AddressId = 2},
                new Order {Id = 4, ProductName="bag", Reciepnt="Amit Doe", Supplier="Sap", Send=DateTime.Parse("2019-09-11") , Delivered=DateTime.Parse("2005-09-17"), AddressId = 2},
                new Order {Id = 5, ProductName="bag", Reciepnt="Monty Cont", Supplier="LuisVitton", Send=DateTime.Parse("2018-08-01") , Delivered=DateTime.Parse("2018-09-11"), AddressId = 3},
                new Order {Id = 6, ProductName="shoes", Reciepnt="Richard Wiil", Supplier="Nike", Send=DateTime.Parse("2005-08-01") , Delivered=DateTime.Parse("2005-09-01"), AddressId = 4},
                new Order {Id = 7, ProductName="t-shirt", Reciepnt="Brad Pitt", Supplier="Adidas", Send=DateTime.Parse("2015-08-01") , Delivered=DateTime.Parse("2015-09-18"), AddressId = 5},
                new Order {Id = 8, ProductName="tabel", Reciepnt="Dan Leo", Supplier="Sed", Send=DateTime.Parse("2009-04-01") , Delivered=DateTime.Parse("2005-05-01"), AddressId = 6},
                new Order {Id = 9, ProductName="tabel", Reciepnt="Matt Deimon", Supplier="Sed", Send=DateTime.Parse("2019-03-01") , AddressId = 7},
                new Order {Id = 10, ProductName="phone", Reciepnt="Pedram feyz", Supplier="Nokia", Send=DateTime.Parse("2019-07-07") , AddressId = 1},
                new Order {Id = 11, ProductName="bottel", Reciepnt="Mary Shel", Supplier="Bav", AddressId = 3},
                new Order {Id = 12, ProductName="towel", Reciepnt="Sara Kenz", Supplier="Sor", AddressId = 8},
            };

            modelBuilder.Entity<Order>().HasData(orders);
        }
    }
}
