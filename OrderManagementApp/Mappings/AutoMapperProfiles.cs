﻿using AutoMapper;
using OrderManagementApp.Dtos;
using OrderManagementApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OrderManagementApp.Mappings
{
    public class AutoMapperProfiles: Profile
    {  
        public AutoMapperProfiles()
        {
            CreateMap<AddressDto, Address>()
                .ForMember(dest => dest.Id, opt => opt.Ignore())
                .ReverseMap()
                .ForMember(dest => dest.Address, opt => {
                    opt.MapFrom(src => src.Street + ", "+ src.City + ", "+ src.State + ", " + src.PostalCode + ", "+  src.Country);
                });

            CreateMap<OrderDto, Order>()
                .ForMember(dest => dest.Id, opt => opt.Ignore())
                .ForMember(dest => dest.Send, opt => { opt.MapFrom(src => src.DispatchDate); })
                .ForMember(dest => dest.Delivered, opt => { opt.MapFrom(src => src.DeliveryDate); })
                .ReverseMap()
                .ForMember(dest => dest.DispatchDate, opt => { opt.MapFrom(src => src.Send); })
                .ForMember(dest => dest.DeliveryDate, opt => { opt.MapFrom(src => src.Delivered); })
                .ForMember(dest => dest.AddressOfDelivery, opt => {
                    opt.MapFrom(src => src.Address.Street + ", " + src.Address.City + ", " + src.Address.State + ", " + src.Address.PostalCode + ", " + src.Address.Country);
                });
        }
    }
}
