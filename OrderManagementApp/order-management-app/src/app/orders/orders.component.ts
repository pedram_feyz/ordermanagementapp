import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.scss']
})
export class OrdersComponent implements OnInit {

  constructor(public router: Router, private route: ActivatedRoute) { }
  orders: any = [];
  ngOnInit() {
    this.route.data.subscribe(data => {
      this.orders = data['orders'];
    });
  }

  navigateWithState(){
    this.router.navigateByUrl('/order/create', { state: { state: 'create' } });
  }

}
