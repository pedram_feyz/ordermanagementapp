import { Component, OnInit, ViewChild } from '@angular/core';
import { Observable } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { Order, OrderModel } from 'src/app/_models/Order';
import { OrderService } from 'src/app/_services/order.service';
import { AddressService } from 'src/app/_services/address.service';
import { AlertifyService } from 'src/app/_services/alertify.service';
import { DatePipe } from '@angular/common'
import { Address } from 'src/app/_models/Address';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-order-edit',
  templateUrl: './order-edit.component.html',
  styleUrls: ['./order-edit.component.scss']
})
export class OrderEditComponent implements OnInit {
  state$: Observable<Order>;
  order: Order;
  selectedAddress: Address;
  addresses: Address[];
  edit: boolean = true;
  @ViewChild('editForm', {
    static: false
  }) editForm: NgForm;

  constructor(public activatedRoute: ActivatedRoute, public router: Router, 
    private orderService: OrderService, private addressService: AddressService , 
    private alertify: AlertifyService, public datepipe: DatePipe) { 

    this.getAllAddresses();
    this.state$ = this.activatedRoute.paramMap
      .pipe(() => window.history.state);
      if(this.state$["state"] == "create"){
        this.edit = false;
        this.order = new OrderModel();
      }
      else if(this.state$["order"] == undefined){
        // TODO: when the page refresh get id of order and put detailes again
        this.router.navigateByUrl('/order');
      }
      else{
        this.order = this.state$["order"];
        this.order.dispatchDate = this.datepipe.transform(this.order.dispatchDate, 'yyyy-MM-dd');
        this.order.deliveryDate = this.datepipe.transform(this.order.deliveryDate, 'yyyy-MM-dd');
      }
    }

  ngOnInit() { }

  // TODO: validation for update and create in fornt-end
  updateOrder(){
    this.order.addressOfDelivery = this.addresses.find(a => a.id == this.order.addressId).address;
    this.orderService.update(this.order.id ,this.order).subscribe(
      next => {
        this.alertify.success('Order updated successfully');
        this.editForm.form.markAsPristine();
      },
      error => {
        this.alertify.errorRange(error.error);
      }
    );
  }

  getAllAddresses(){
    this.addressService.getAll().subscribe(
      next => {
        this.selectedAddress = next.find( x => x.id == this.order.addressId);
        this.addresses = next;
      },
      error => {
        this.alertify.errorRange(error.error);
      }
    );
  }

  createOrder(){
    this.orderService.create(this.order).subscribe(
      next => {
        this.alertify.success('Order created successfully');
        this.editForm.reset();
      },
      error => {
        this.alertify.errorRange(error.error);
      }
    );
  }

}
