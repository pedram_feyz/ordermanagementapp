import { Component, OnInit, Input } from '@angular/core';
import { Router } from "@angular/router";
import { Order } from '../../_models/Order';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.scss']
})
export class OrderComponent implements OnInit {

  @Input() order: Order;
  
  constructor(public router: Router) { }
  ngOnInit() {
  }

  navigateWithState(order : Order) {
    this.router.navigateByUrl('/order/edit', { state: { order: order } });
  }

}
