import { Component, OnInit, ViewChild } from '@angular/core';
import { Observable } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { Address, AddressModel } from 'src/app/_models/Address';
import { AddressService } from 'src/app/_services/address.service';
import { AlertifyService } from 'src/app/_services/alertify.service';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-address-edit',
  templateUrl: './address-edit.component.html',
  styleUrls: ['./address-edit.component.scss']
})
export class AddressEditComponent implements OnInit {
  state$: Observable<Address>;
  address: Address;
  edit: boolean = true;
  @ViewChild('editForm', {
    static: false
  }) editForm: NgForm;
  
  constructor(public activatedRoute: ActivatedRoute, public router: Router, 
    private addressService: AddressService, private alertify: AlertifyService) { }

  ngOnInit() {
    this.state$ = this.activatedRoute.paramMap
      .pipe(() => window.history.state);
      if(this.state$["state"] == "create"){
        this.edit = false;
        this.address = new AddressModel();
      }
      else if(this.state$["address"] == undefined){
        // TODO: when the page refresh get id of address and put detailes again
        this.router.navigateByUrl('/address');
      }
      else{
        this.address = this.state$["address"];
      }
      
  }

  // TODO: validation for update and create in fornt-end
  updateAddress(){
    this.addressService.update(this.address.id ,this.address).subscribe(
      next => {
        this.alertify.success('Address updated successfully');
        this.editForm.form.markAsPristine();
      },
      error => {
        this.alertify.errorRange(error.error);
      }
    );
  }

  createAddress(){
    this.addressService.create(this.address).subscribe(
      next => {
        this.alertify.success('Address created successfully');
        this.editForm.reset();
      },
      error => {
        this.alertify.errorRange(error.error);
      }
    );
  }

}
