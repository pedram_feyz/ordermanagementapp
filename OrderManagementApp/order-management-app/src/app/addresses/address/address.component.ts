import { Component, OnInit, Input } from '@angular/core';
import { Router } from "@angular/router";
import { Address } from '../../_models/Address';

@Component({
  selector: 'app-address',
  templateUrl: './address.component.html',
  styleUrls: ['./address.component.scss']
})
export class AddressComponent implements OnInit {
  @Input() address: Address;
  
  constructor(public router: Router) { }
  ngOnInit() {
  }

  navigateWithState(address : Address) {
    this.router.navigateByUrl('/address/edit', { state: { address: address } });
  }

}
