import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-addresses',
  templateUrl: './addresses.component.html',
  styleUrls: ['./addresses.component.scss']
})
export class AddressesComponent implements OnInit {

  constructor(public router: Router, private route: ActivatedRoute) { }
  addresses: any = [];
  ngOnInit() {
    this.route.data.subscribe(data => {
      this.addresses = data['addresses'];
    });
  }

  navigateWithState(){
    this.router.navigateByUrl('/address/create', { state: { state: 'create' } });
  }

}
