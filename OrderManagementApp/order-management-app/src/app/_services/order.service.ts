import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import {  Order } from '../_models/Order';

@Injectable({
  providedIn: 'root'
})
export class OrderService {
  baseUrl = environment.apiUrl;

  constructor(private http: HttpClient) { }

  get(id): Observable<Order>{
    return this.http.get<Order>(this.baseUrl + 'orders/' + id);
  }

  getAll(): Observable<Order[]>{
    return this.http.get<Order[]>(this.baseUrl + 'orders');
  }

  update(id: number ,address: Order): Observable<object>{
    return this.http.put(this.baseUrl + 'orders/' + id, address)
  }

  create(address: Order): Observable<object>{
    return this.http.post(this.baseUrl + 'orders/', address)
  }
}
