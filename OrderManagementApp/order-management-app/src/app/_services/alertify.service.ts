import { Injectable } from '@angular/core';
import * as alertify from '../../../node_modules/alertifyjs/build/alertify.js'
//declare let alertify: any;

@Injectable({
  providedIn: 'root'
})

export class AlertifyService {

  constructor() { }

  confirm(message: string, okCallBack: () => any) {
    alertify.confirm(message, function(e) {
        if (e) {
            okCallBack();
        } else {
        }
    });
}

success(message: string) {
    alertify.success(message);
}

error(message: string) {
    alertify.error(message);
}

warning(message: string) {
    alertify.warning(message);
}

message(message: string) {
    alertify.message(message);
}

errorRange(errors){
    if(typeof errors == "string"){
        this.error(errors);
    }
    else if(typeof errors == "object"){
        let error = this.error;
        Object.keys(errors).forEach(function(key,index) {
            errors[key].forEach((er) => error(er))
        });
    }
}

}
