import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Address } from '../_models/Address'

@Injectable({
  providedIn: 'root'
})
export class AddressService {
  baseUrl = environment.apiUrl;

  constructor(private http: HttpClient) { }

  get(id): Observable<Address>{
    return this.http.get<Address>(this.baseUrl + 'addresses/' + id);
  }

  getAll(): Observable<Address[]>{
    return this.http.get<Address[]>(this.baseUrl + 'addresses');
  }

  update(id: number ,address: Address): Observable<object>{
    return this.http.put(this.baseUrl + 'addresses/' + id, address)
  }

  create(address: Address): Observable<object>{
    return this.http.post(this.baseUrl + 'addresses/', address)
  }
}

