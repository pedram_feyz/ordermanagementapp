import { Injectable } from '@angular/core';
import { Address } from '../_models/Address';
import { Resolve, Router, ActivatedRouteSnapshot } from '@angular/router';
import { AddressService } from '../_services/address.service';
import { AlertifyService } from '../_services/alertify.service';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class AddressListResover implements Resolve<Address> {
  constructor(private addressService: AddressService, private router: Router, private alertify: AlertifyService) {}

  resolve(route: ActivatedRouteSnapshot): Observable<Address> {
    return this.addressService.getAll().pipe(
      catchError(error => {
        this.alertify.error('Problem retrieving data');
        this.router.navigate(['/address']);
        return of(null);
      })
    );
  }

}