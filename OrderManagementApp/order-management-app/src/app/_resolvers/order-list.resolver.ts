import { Injectable } from '@angular/core';
import { Order } from '../_models/Order';
import { Resolve, Router, ActivatedRouteSnapshot } from '@angular/router';
import { OrderService } from '../_services/order.service';
import { AlertifyService } from '../_services/alertify.service';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})

export class OrderListResover implements Resolve<Order> {
  constructor(private orderService: OrderService, private router: Router, private alertify: AlertifyService) {}

  resolve(route: ActivatedRouteSnapshot): Observable<Order> {
    return this.orderService.getAll().pipe(
      catchError(error => {
        this.alertify.error('Problem retrieving data');
        this.router.navigate(['/order']);
        return of(null);
      })
    );
  }

}