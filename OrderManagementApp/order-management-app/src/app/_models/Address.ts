export interface Address {
    id: number;
    country: string;
    city: string;
    state: string;
    PostalCode: number;
    street: string;
    created: Date;
    lastActive: Date;
    address: string;
}

export class AddressModel {
    id: number;
    country: string;
    city: string;
    state: string;
    PostalCode: number;
    street: string;
    created: Date;
    lastActive: Date;
    address: string;
}