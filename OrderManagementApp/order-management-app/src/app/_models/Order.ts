export interface Order {
    id: number;
    reciepnt: string;
    supplier: string;
    productName: string;
    dispatchDate: string;
    deliveryDate: string;
    addressOfDelivery: string;
    addressId: number;
}

export class OrderModel {
    id: number;
    reciepnt: string;
    supplier: string;
    productName: string;
    dispatchDate: string;
    deliveryDate: string;
    addressOfDelivery: string;
    addressId: number;
}