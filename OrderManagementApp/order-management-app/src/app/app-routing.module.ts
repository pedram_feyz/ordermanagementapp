import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AddressesComponent } from './addresses/addresses.component';
import { AddressEditComponent} from './addresses/address-edit/address-edit.component';
import { OrdersComponent } from './orders/orders.component';
import { HomeComponent } from './home/home.component';
import { AddressListResover } from './_resolvers/address-list.resolver';
import { OrderListResover } from './_resolvers/order-list.resolver';
import { OrderEditComponent } from './orders/order-edit/order-edit.component';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full'},
    { path: 'home', component: HomeComponent},
    {
      path: '',
      runGuardsAndResolvers: 'always',
      // use resolve to load related data instead of doing in ngOnInit
      children: [
        { path: 'order', component: OrdersComponent, resolve: {orders: OrderListResover}},
        { path: 'order/edit', component: OrderEditComponent},
        { path: 'order/create', component: OrderEditComponent},
        { path: 'address', component: AddressesComponent, resolve: {addresses: AddressListResover}},
        { path: 'address/edit', component: AddressEditComponent},
        { path: 'address/create', component: AddressEditComponent}
      ]
    },
    { path: '**', redirectTo: 'home', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
