import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpClientModule} from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { FormsModule } from '@angular/forms';
import { NavComponent } from './nav/nav.component';
import { OrdersComponent } from './orders/orders.component';
import { AddressesComponent } from './addresses/addresses.component';
import { HomeComponent } from './home/home.component';
import { AddressComponent } from './addresses/address/address.component';
import { AddressEditComponent } from './addresses/address-edit/address-edit.component';
import { OrderComponent } from './orders/order/order.component';
import { OrderEditComponent } from './orders/order-edit/order-edit.component';
import { DatePipe } from '@angular/common';

@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    OrdersComponent,
    AddressesComponent,
    HomeComponent,
    AddressComponent,
    AddressEditComponent,
    OrderComponent,
    OrderEditComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [DatePipe],
  bootstrap: [AppComponent]
})
export class AppModule { }
