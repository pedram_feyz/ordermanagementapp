			# OrderManagementApp

			How to run application locally:
			
			clone application from Bitbucket:
			
			https://bitbucket.org/pedram_feyz/ordermanagementapp/src/master/
			git clone https://bitbucket.org/pedram_feyz/ordermanagementapp.git
			
			1)  OrderManagementApp\order-management-app>npm install       (get dependencies)
			2)  OrderManagementApp\order-management-app>npm run-script build    (Generating ES5 bundles which goes to wwwroot folder)
			3)  OrderManagementApp>dotnet run 
			
			http://localhost:5000/home
			

			what is done:
			
			deploy App in AWS(elasticbeanstalk)
			http://ordermanagementapp-prod.ap-southeast-2.elasticbeanstalk.com/address
			
			Implement N-Tier Architecture with control layer, business logic layer, and repo layer
			
			Setup Entity Framework
			
			Design database with a one-to-many relationship
			
			Add DbContext and register DbSets in it
			
			Create database and tables and data seeding (code-first approach)
			
			Use built-in Dependency Injection in .net core and register services and Dbcontext and repo services (for Ioc)
			
			Use generic repo service to be used by either Order and Address
			
			Set up Automapper and mapping Dtos with entities
			
			Add global exception handler as middleware
			
			what is left:
			Please note that the focus has been on structure and implementation than styling and details
			Some validation stuff in front-end and back-end
			Pagination and searching(filtering) for order and address
			Custom error handling in the back-end to return proper status rather than 500
			Some styling, user experience
			A smoke test is done but the edge case scenario has not been covered
			
			
			
			
